from time import sleep
import pandas as pd
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import time

start_time = time.time()
driver = webdriver.Chrome(executable_path="C://chromedriver_win32 (2)//chromedriver.exe")
driver.maximize_window()

df_covid_data = pd.DataFrame(columns=['Resource', 'Overlay', 'Contact_name', 'Phone_no', 'Description'])


def check_element(element_class, element_name):
    try:
        ret_val = element_class.find_element_by_class_name(element_name).text
    except:
        ret_val = 'NA'
    return ret_val


driver.get("https://verifiedcovidleads.com/")
sleep(2)
tabBar_buttons = driver.find_element_by_id('tabBar').find_elements_by_tag_name('button')
for i in tabBar_buttons:
    resource_val = i.text
    print(resource_val)
    i.click()

    category_counter = driver.find_elements_by_xpath("//div[@class ='bottom-area']")
    try:
        # category_counter = driver.find_elements_by_xpath("//div[@class ='bottom-area']")
        driver.find_element_by_xpath("//div[@class ='bottom-area']").text
    except:
        category_counter = None

    while category_counter is not None:
        # Loop to get the struct
        previous_element = check_element(category_counter[0], 'card-title')
        previous_category_counter = category_counter

        for catCount in category_counter:
            df_covid_data.loc[len(df_covid_data)] = [resource_val,
                                                     check_element(catCount, 'bottom-right-overlay'),
                                                     check_element(catCount, 'card-header'),
                                                     check_element(catCount, 'card-title'),
                                                     check_element(catCount, 'card-subtitle')
                                                     ]
        driver.execute_script("arguments[0].scrollIntoView();", category_counter[len(category_counter) - 1])

        category_counter = driver.find_elements_by_xpath(".//div[@class ='bottom-area']")

        # Check if more div elements are present - to be scrolled through
        if previous_element == check_element(category_counter[0],
                                             'card-title') and previous_category_counter == category_counter:
            category_counter = None
        else:
            category_counter = driver.find_elements_by_xpath(".//div[@class ='bottom-area']")

df_covid_data.drop_duplicates(subset=['Resource', 'Overlay', 'Contact_name', 'Phone_no', 'Description'], inplace=True)

df_covid_data.sort_values(["Resource", "Overlay"], axis=0,
                          ascending=[True, False], inplace=True)

df_covid_data.to_csv('output_pan.csv')

driver.quit()

end_time = time.time()
print(end_time - start_time)
